package br.com.mastertech.imersivo.estacionamento.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.mastertech.imersivo.estacionamento.model.Carro;
import br.com.mastertech.imersivo.estacionamento.model.Estado;

public interface CarroRepository extends CrudRepository<Carro, Long> {

	Carro findByPlacaAndEstado(String placa, Estado estado);
	
}
